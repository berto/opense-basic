How to use OpenSE BASIC
-----------------------

This package contains two files:

- opense.rom: OpenSE BASIC, a replacement for the ZX Spectrum ROM.
- opense-stub.rom: Stub to run OpenSE BASIC in multi-ROM machines.

This is the list of ROMs used by each Spectrum model, as they appear
in the spectrum-roms package:

- ZX Spectrum 16K/48K:
  48.rom

- Timex TC2048:
  tc2048.rom

- ZX Spectrum 128K:
  ROM-0: 128-0.rom
  ROM-1: 128-1.rom

- ZX Spectrum +2:
  ROM-0: plus2-0.rom
  ROM-1: plus2-1.rom

- ZX Spectrum +2A/+3:
  ROM-0: plus3-0.rom
  ROM-1: plus3-1.rom
  ROM-2: plus3-2.rom
  ROM-3: plus3-3.rom

To use OpenSE BASIC in the Spectrum 16K, 48K and TC2048 you just need
to load opense.rom instead of the original ROM.

To use OpenSE BASIC in the Spectrum 128K and +2, you have to put
opense-stub.rom in ROM-0 and opense.rom in ROM-1.

To use OpenSE BASIC in the Spectrum +2A and +3, you have to put
opense-stub.rom in ROM-0 and ROM-2, and opense.rom in ROM-1 and ROM-3.

It's also possible to use OpenSE BASIC and the original Spectrum ROMs
at the same time in the 128K models. Refer to the OpenSE BASIC Quick
Reference manual included in this package for more information.

 -- Alberto Garcia <berto@igalia.com>, Thu,  2 Jun 2011 23:32:48 +0300
