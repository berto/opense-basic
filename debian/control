Source: opense-basic
Section: otherosfs
Priority: optional
Maintainer: Alberto Garcia <berto@igalia.com>
Build-Depends: debhelper-compat (= 12), pasmo
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://zxdesign.itch.io/opense
Vcs-Browser: https://salsa.debian.org/berto/opense-basic
Vcs-Git: https://salsa.debian.org/berto/opense-basic.git

Package: opense-basic
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Free software ROM for the Sinclair ZX Spectrum
 OpenSE BASIC is a replacement ROM for the ZX Spectrum for use with
 emulators or real machines.
 .
 Some of the highlights include a faster and better version of
 Sinclair BASIC, an enhanced editor, AY support, ULAplus support,
 improved SCREEN$ handling for UDGs and 8-bit character sets and a
 faster and more accurate floating-point library.
 .
 OpenSE BASIC remains compatible with the majority of Spectrum
 software and hardware.
 .
 It can replace the original ROM in 16K/48K models out of the box.
 It's also possible to run it in 128K models, but note that it does
 not include many functionalities present in the original ROMs of
 those machines, such as the 128K menu, editor and +3DOS.
