;; Stub ROM to use OpenSE BASIC in 128K models
org 0

;; Select ROM 1 (the 48K BASIC ROM, which OpenSE BASIC replaces)
ld bc, 7ffdh
ld a, 10h
out (c), a

;; Fill with zeroes up to 16384 bytes
rept 3ff9h
defb 0
endm
